resource "yandex_compute_instance" "backend1" {
  name = "backend1"
  hostname = "backend1"
  allow_stopping_for_update = true
  platform_id = "standard-v3"
  zone = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
    core_fraction = 20
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.centos-8.id
    }
  }

  network_interface {
    # Указан id подсети default-ru-central1-a
    subnet_id = yandex_vpc_subnet.wp-subnet-a.id
    nat       = true
  }

  metadata = {
     ssh-keys = "cloud-user:${file("~/.ssh/id_rsa_testya.pub")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "backend2" {
  name = "backend2"
  hostname = "backend2"
  allow_stopping_for_update = true
  platform_id = "standard-v3"
  zone = "ru-central1-b"


  resources {
    cores  = 2
    memory = 2
    core_fraction = 20
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.centos-8.id
    }
  }

  network_interface {
    # Указан id подсети default-ru-central1-a
    subnet_id = yandex_vpc_subnet.wp-subnet-b.id
    nat       = true

  }

  metadata = {
     ssh-keys = "cloud-user:${file("~/.ssh/id_rsa_testya.pub")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "frontend" {
  name = "frontend"
  hostname = "frontend"
  allow_stopping_for_update = true
  platform_id = "standard-v3"
  zone = "ru-central1-a"

  resources {
    cores  = 2
    memory = 1
    core_fraction = 20
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.centos-8.id
    }
  }

  network_interface {
    # Указан id подсети default-ru-central1-a
    subnet_id = yandex_vpc_subnet.wp-subnet-a.id
    nat       = true
  }

  metadata = {
     ssh-keys = "cloud-user:${file("~/.ssh/id_rsa_testya.pub")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "backup" {
  name = "backup"
  hostname = "backup"
  allow_stopping_for_update = true
  platform_id = "standard-v3"
  zone = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
    core_fraction = 20
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.centos-8.id
    }
  }

  network_interface {
    # Указан id подсети default-ru-central1-a
    subnet_id = yandex_vpc_subnet.wp-subnet-a.id
    nat       = true
  }

  metadata = {
     ssh-keys = "cloud-user:${file("~/.ssh/id_rsa_testya.pub")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "monitoring" {
  name = "monitoring"
  hostname = "monitoring"
  allow_stopping_for_update = true
  platform_id = "standard-v3"
  zone = "ru-central1-a"

  resources {
    cores  = 2
    memory = 4
    core_fraction = 50
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.centos-8.id
    }
  }

  network_interface {
    # Указан id подсети default-ru-central1-a
    subnet_id = yandex_vpc_subnet.wp-subnet-a.id
    nat       = true
  }

  metadata = {
     ssh-keys = "cloud-user:${file("~/.ssh/id_rsa_testya.pub")}"
  }
  scheduling_policy {
    preemptible = true
  }
}