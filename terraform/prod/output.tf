
output "backend1_public_ip_address" {
  description = "Virtual machine IP"
  value = yandex_compute_instance.backend1.network_interface[0].nat_ip_address
}

output "backend2_public_ip_address" {
  description = "Virtual machine IP"
  value = yandex_compute_instance.backend2.network_interface[0].nat_ip_address
}

output "frontend_public_ip_address" {
  description = "Virtual machine IP"
  value = yandex_compute_instance.frontend.network_interface[0].nat_ip_address
}

output "backup_public_ip_address" {
  description = "Virtual machine IP"
  value = yandex_compute_instance.backup.network_interface[0].nat_ip_address
}

output "monitoring_public_ip_address" {
  description = "Virtual machine IP"
  value = yandex_compute_instance.monitoring.network_interface[0].nat_ip_address
}