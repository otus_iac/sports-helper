# Проект. Веб приложение sports-helper

## 1. Подготовка локальной среды

Подымаем тестовый контур через docker compose на локальной машине

```bash
docker compose up
```

Проверим корректность работы зайдя на <http://localhost:3000>  
Если все поднялось, создаём папку build командой,  
это необходимо для дальнейшего создания prod:  

```bash
docker compose exec frontend npm run build
```

Будет создана папка client/build, в ней будет код веб приложения sports-helper (необходимый для frontend nginx).  
Для удаления контейнеров используется команда:  

```bash
docker compose down -v
```

## 2. Описание веб приложения sports-helper (режим разработчика)

Для работы приложения sports-helper необходимы:

- frontend на reactjs
- backend на nodejs
- mongodb

### 2.1 Frontend

Папка client:

```txt
 client
│   ├── babel-plugin-macros.config.js
│   ├── build
│   ├── Dockerfile
│   ├── jsconfig.json
│   ├── node_modules
│   ├── package.json
│   ├── package-lock.json
│   ├── public
│   ├── README.md
│   └── src
```

- package.json - основной json файл для установки пакетов для node. Также в этом файле указаны параметры запуска нашего приложения.

```js
 "proxy": "http://api:4200",
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test",
    "eject": "react-scripts eject"
  }
```

proxy": "<http://api:4200>", `api` это имя контейнера с бэкендом  
"start": "react-scripts start", скрипт старта приложения в dev среде. Располагаются в папке node_modules (в контейнере)  
"build": "react-scripts build", скрипт для создания папки build для prod среды.  

- package-lock.json json файл для фиксации версий зависимостей.
- папка build это рабочий код приложения, папка создаётся при запуске команды `docker compose exec frontend npm run build`.  
  данная папка копируется на сервер nginx.  
- папка node modules пустая папка, эта папка куда инсталлируются  
  пакеты npm (у нас она существует внутри контейнера frontend)  
- Dockerfile - файл для создания контейнера frontend.  
 [Dockerfile](./client/Dockerfile)  
- папка public - содержит index.html и картинки
- папка src содержит js код приложения

### 2.2 Backend

Папка server:

```txt
├── server
│   ├── controllers
│   ├── core
│   ├── Dockerfile
│   ├── models
│   ├── node_modules
│   ├── package.json
│   ├── package-lock.json
│   ├── server.ts
│   ├── tsconfig.json
│   ├── utils
│   └── yarn.lock
|   └── .env
```

- package.json основной файл для запуска api backend.

```js
 "scripts": {
    "start": "ts-node server.ts",
    "dev": "ts-node server.ts"
  },
```

- server.ts - основой файл с кодом приложения, происходит вызов функций.
- папка node modules пустая, эта папка куда инсталируются пакеты npm (у нас она существует внутри контейнера api)
- .env файл содержит переменные для запуска нашего приложения.
- Dockerfile - файл для создания контейнера api.
   [Dockerfile](./server/Dockerfile)

- server/core/db.ts - содержит параметры подключения backend к базе данных mongo.

### 2.3 Mongo

Для работы Backend необходим сервис mongodb не старше 5 ой версии.

### 2.4 Запуск сервиса в dev режиме

Сервис в dev режиме запускается с помощью docker compose.

```bash
docker compose up
```

Docker-compose:

```yml
version: '3.8'
services:
  frontend:
    build: ./client
    restart: always
    ports:
      - '3000:3000'
    depends_on:
      - api
    volumes:
     - frontend_node_modules:/app/node_modules
     - ./client:/app
    networks:
      - my_network
  api:
    build: ./server
    restart: always
    ports:
      - '4200:4200'
    depends_on:
      - mongo
    volumes:
     - api_node_modules:/app/node_modules
     - ./server:/app
    networks:
      - my_network
  mongo:
    image: mongo:5.0.23-rc0
    restart: always
    ports:
     - 8081:8081
    volumes:
     - mongo_data:/data/db
    networks:
      - my_network
  my_network:
    driver: bridge
    ipam:
      driver: default
      config:
        - subnet: 172.25.0.0/16        
volumes:
  mongo_data:
  frontend_node_modules:  
  api_node_modules:
```

Ссылка по которой будет доступно приложение:

<http://localhost:3000>

## 3. Запуск проекта в yandex cloud

### 3.1 Подготовим локальный компьютер

Запустим local.yml

```bash
ansible-playbook local.yml --ask-become
```

```yml
---
- import_playbook: ./local_playbooks/mongo_key.yml 
- import_playbook: ./local_playbooks/nginx_cert.yml
```

Будут выполнены действия на localhost:

- сгенерирован секретный ключ `mongo_key` необходимый для репликации серверов mongo
- сгененирован самоподписанный сертификат для сервера nginx

После этого можно сделать git push.

### 3.2 Запуск серверов в Yandec Cloud

Установка всех серверов проиcходит с помощью gitlab pipeline.

Файл .gitlab-ci.yml

```yml
stages:   
    - check terraform
    - check ansible
    - stage_deploy_infra
    - stage_deploy_app
    - stage_destroy
    - prod_deploy_infra
    - prod_deploy_app
    - prod_destroy

.terraform_prepare: &tf_setup 
  before_script:
      - mkdir ~/.ssh/
      - echo $ID_RSA_PUB > ~/.ssh/id_rsa_testya.pub
    

.ansible: &ansible
  before_script:
    - mkdir ~/.ssh/
    - echo "$ID_RSA" | base64 -d > ~/.ssh/id_rsa_testya
    - chmod -R 700 ~/.ssh    
    - cd ansible
    - chmod o-w .

variables:
    TF_VAR_yc_token: $yc_token
    TF_VAR_yc_cloud: $yc_cloud

Check terraform files:
    stage: check terraform
    variables:
      TF_VAR_yc_folder: $yc_folder_prod
    image:
      name: tolik1717/terraform:1.6
      entrypoint:
        - '/usr/bin/env'
        - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
        
    <<:  *tf_setup
    script:
        - echo "checks terraform stage files"
        - cd terraform/stage 
        - terraform init       
        - tflint --var-file=/dev/null
        - terraform validate
        - terraform plan
        - echo "checks terraform stage files"
        - cd ../prod
        - terraform validate
        - terraform plan

    when: manual     
    only: 
        - main

Deploy infrustructure stage:
    stage: stage_deploy_infra
    variables:
      TF_VAR_yc_folder: $yc_folder_stage
    image:
      name: tolik1717/terraform:1.6
      entrypoint:
        - '/usr/bin/env'
        - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
    <<:  *tf_setup
    script:
        - echo "$TF_VAR_yc_folder"
        - cd terraform/stage
        - terraform init
        - terraform apply --auto-approve
    when: manual        
    only: 
        - main


Deploy infrustructure prod:
    stage: prod_deploy_infra
    variables:
      TF_VAR_yc_folder: $yc_folder_prod
    image:
      name: tolik1717/terraform:1.6
      entrypoint:
        - '/usr/bin/env'
        - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
    <<:  *tf_setup
    script:
        - echo "$TF_VAR_yc_folder"
        - cd terraform/prod
        - terraform init
        - terraform apply --auto-approve
    when: manual        
    only: 
        - main

Destroy infrustructure stage:
    stage: stage_destroy
    variables:
      TF_VAR_yc_folder: $yc_folder_stage
    image:
      name: tolik1717/terraform:1.6
      entrypoint:
        - '/usr/bin/env'
        - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
    <<:  *tf_setup
    script:
        - cd terraform/stage
        - terraform init        
        - terraform destroy --auto-approve
    when: manual
    only: 
        - main


Destroy infrustructure prod:
    stage: prod_destroy
    variables:
      TF_VAR_yc_folder: $yc_folder_prod
    image:
      name: tolik1717/terraform:1.6
      entrypoint:
        - '/usr/bin/env'
        - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
    <<:  *tf_setup
    script:
        - cd terraform/prod
        - terraform init        
        - terraform destroy --auto-approve
    when: manual
    only: 
        - main

Check ansible:
    stage: check ansible
    image: tolik1717/ansible_runner:1.2
    <<: *ansible
    script:
        - ansible --version
        - ansible-lint -v *.yml
    when: manual     
    only:
        - main

Install application stage:
    stage: stage_deploy_app
    variables:
      TF_VAR_yc_folder: $yc_folder_stage
    image: tolik1717/ansible_runner:1.2
    <<: *ansible
    script:
        - ansible --version
        - ansible-playbook infra.yml
    when: manual        
    only:
        - main

Install application prod:
    stage: prod_deploy_app
    variables:
      TF_VAR_yc_folder: $yc_folder_prod
    image: tolik1717/ansible_runner:1.2
    <<: *ansible
    script:
        - ansible --version
        - ansible-playbook infra.yml
    when: manual        
    only:
        - main
```

Стадии CI/CD

![Alt text](images/stage-ci-cd.png)
![Alt text](image.png)

![Alt text](images/prod-ci-cd.png)

- проверка кода terrafrom
- проверка кода ansible
- создание инфраструктуры stage серверов в yandex cloud
- загрузка приложения на stage сервера yandex cloud
- удаление инфраструктуры stage серверов в yandex cloud
- создание инфраструктуры prod серверов в yandex cloud
- загрузка приложения на prod сервера yandex cloud
- удаление инфраструктуры prod серверов в yandex cloud

Схема работы серверов:

![Alt text](images/scheme.png)

После запуска всех сервисов приложение sports-helper будет доступно по адресу:

<https://sports-helper>

Мониторинг будет доступен по адресу:

<http://monitoring:3000>

Сервер prometheus будет доступен по адресу:

<http://monitoring:9090>

Логи располагаются на сервере backup.

Проверка существования логов:

```bash
cd ansible
export TF_VAR_yc_token=`yc config get token`
export TF_VAR_yc_folder="folder id"
ansible-inventory --list
ansible-playbook install.yml --tags=check-logs -l backup
```

Проверка существования бэкапов:  
Бэкапы базы mongo располагаются на сервере backup:

```bash
cd ansible
ansible-playbook install.yml --tags=show-backups -l backup
```

Проверка состояния репликации mongodb:

```bash
ansible-playbook install.yml --tags=show_repl -l backend1
```

Бэкапом кода приложения является git репозитарий.  
На всех серверах запущена система безопасности selinux в режиме enforcing, а также запущен firewall.

### 3.3 Описание контейнеров Gitlab CI/CD

Контейнер для проверки terraform кода.

Файл Dockerfile_terraform

```Dockerfile
FROM hashicorp/terraform:1.6
ADD https://github.com/terraform-linters/tflint/releases/latest/download/tflint_linux_amd64.zip /usr/local/bin/tflint.zip
RUN unzip /usr/local/bin/tflint.zip -d /usr/local/bin && \
    rm /usr/local/bin/tflint.zip
ENV PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
```

```bash
cd ci-cd
docker login
docker build -t tolik1717/terraform:1.6 -f Dockerfile_terraform .
docker push tolik1717/terraform:1.6
```

Контейнер для проверки ansible кода и работы в yandex облаке:

Файл Dockerfile_ansible

```Dockerfile
# Download base image ubuntu 20.04
FROM ubuntu:20.04
# Configure tz-data
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
# Install dependencies
RUN apt-get update -y && \
    apt-get install -y software-properties-common && \
    apt-add-repository -y ppa:ansible/ansible && \
    apt-get update -y && \
    apt-get install -y rsync python3 python3-pip git ansible && \
    pip3 install ansible-lint==4.0.0 yandexcloud
# CMD command
CMD ["/bin/bash"]
```

### 3.4 Подробное описание установки приложения с помощью ansible

Динамический инвентарь ansible для yandex cloud - файл `yc2inv.py`  
Для использования необходимы переменные (TF_VAR_yc_token, TF_VAR_yc_folder):  

```bash
cd ansible
export TF_VAR_yc_token=`yc config get token`
export TF_VAR_yc_folder="folder id"
ansible-inventory --list
```

ansible.cfg:

```txt
[defaults]
host_key_checking = false
inventory         =./yc2inv.py, hosts
roles_path            =./roles
private_key_file = ~/.ssh/id_rsa_testya
remote_user = cloud-user
ANSIBLE_SSHKEY=/ansible/id_rsa_testya
```

В файле hosts содержаться статические имена групп.

### 3.4.1 Основной playbook по установке приложения

Infra.yml имортирует отдельные playbook для настройки каждого хоста:

Infra.yml

```yml
---

- name: monitoring
  import_playbook: monitoring.yml

- name: backend2
  import_playbook: backend2.yml

- name: backup
  import_playbook: backup.yml

- name: backend1
  import_playbook: backend1.yml

- name: restart backend2 sports-helper service
  import_playbook: restart_backend2.yml

- name: frontend
  import_playbook: frontend.yml
```

**Сервер мониторинга monitoring.yml:**

- rsyslog-client.yml - настройка клиента по отправки логов rsyslog
- install_prom.yml - установка prometheus
- install_grafana.yml - установка grafana
- settings_grafana.yml - настройка grafana
- install_node_exp.yml - установка node exporter для prometheus
- firewall_on.yml - включение и настройка firewall. Разрешенные порты указаны в папке groups_vars.

**Сервер бэкенда 2 backend2.yml:**

- rsyslog-client.yml - настройка клиента по отправки логов rsyslog
- install_mongo.yml - установка mongodb
- enable_auth.yml - создание служебных пользователей и включение авторизации mongo
- mongo_conf.yml - генерация конфига базы mongo
- install node.yml - установка node
- create_service.yml - создание сервиса backend sports-helper
- copy_app.yml - копирование папки server - рабочего кода приложения backend
- install_node_exp.yml - установка node exporter для prometheus
- firewall_on.yml - включение и настройка firewall. Разрешенные порты указаны в папке groups_vars.

**Сервер бэкапа backup.yml:**

- rsyslog-srv.yml - настройка сервера по приему логов rsyslog
- install_mongo.yml - установка mongodb
- enable_auth.yml - создание служебных пользователей и включение авторизации mongo
- mongo_conf.yml - генерация конфига базы mongo
- backup - создания сервиса планировщика по выполнению бэкапов с помощью mongodump
- install_node_exp.yml - установка node exporter для prometheus
- firewall_on.yml - включение и настройка firewall. Разрешенные порты указаны в папке groups_vars.

**Сервер бэкенда 1 backend1.yml:**

- rsyslog-client.yml - настройка клиента по отправки логов rsyslog
- install_mongo.yml - установка mongodb
- enable_auth.yml - создание служебных пользователей и включение авторизации mongo
- mongo_conf.yml - генерация конфига базы mongo
- enable_repl.yml - включение репликации базы mongo, запускается на основном primary участнике репликации,  
  необходимо запускать,когда все остальные участники репликации существуют, при восстановлении сервера primary, данный playbook желательно не использовать.
- install node.yml - установка node
- create_service.yml - создание сервиса backend sports-helper
- copy_app.yml - копирование папки server - рабочего кода приложения backend
- install_node_exp.yml - установка node exporter для prometheus
- firewall_on.yml - включение и настройка firewall. Разрешенные порты указаны в папке groups_vars.

**Перезапуск restart_backend2.yml:**

- restart_backend2.yml перезапускает приложение sports-helper на сервере backend2, это необходимо  
  для корректной работы приложения после включения репликации на всех серверах кластера mongo.

**Сервер фронтенда frontend.yml:**

- rsyslog-client.yml - настройка клиента по отправки логов rsyslog
- install_nginx.yml - установка nginx
- syn_fold - синхронизация папки build frontend
- nginx_cfg.yml - генерация nginx cfg
- nginx_selinux - настройка модуля безопасности selinux
- install_node_exp.yml - установка node exporter для prometheus
- firewall_on.yml - включение и настройка firewall. Разрешенные порты указаны в папке groups_vars.

### 3.4.2 Описание ролей и тэгов

Роли ansible настроены таким образом, чтобы работало наследование тегов при запуске playbook с тегами.
Любую роль можно установить вызвав ее с помощью ansible-playbook и необходимыми тэгами (для удобства использования в провижионерах)

Основной playbook для ролей:  

Install.yml

```yml
---
- name: Install roles
  hosts: all
  become: yes
  gather_facts: true
  roles:
    - monitoring
    - logs
    - nginx
    - mongodb
    - nodejs
    - backup
    - firewalld
```

**Роль nginx теги:**

- install_nginx - установка nginx
- nginx_cfg - генерация nginx cfg
- syn_fold - синхронизация папки build frontend
- nginx_selinux - настройка модуля безопасности selinux

**Роль nodejs теги:**

- install node - установка node
- copy_app - копирование папки server - рабочего кода приложения backend
- create_service - создание сервиса backend sports-helper
- restart - перезапуск службы sports-helper

**Роль mongodb теги:**

- install_mongo - установка mongodb
- enable_auth - создание служебных пользователей и включение авторизации mongo
- mongo_conf - генерация конфига базы mongo
- enable_repl - включение репликации базы mongo, запускается на основном primary участнике репликации,  
  необходимо запускать,когда все остальные участники репликации существуют, при восстановлении сервера primary, данный тег желательно не использовать
- show_repl - проверка состояния репликации mongodb.

**Роль backup теги:**

- backup - создания сервиса планировщика по выполнению бэкапов с помощью mongodump
- show-backups -  проверка существования бэкапов.

**Роль logs теги:**

- rsyslog-client - настройка клиента по отправки логов rsyslog
- rsyslog-srv - настройка сервера по приему логов rsyslog
- check-logs - проверка существования логов на сервере rsylog.

**Роль monitoring теги:**

- install_prom - установка prometheus
- srv_prom_cfg - настройка конфига prometheus
- install_node_exp - установка node exporter для prometheus
- install_grafana - установка grafana
- settings_grafana - настройка grafana.

**Роль firewalld теги:**

- firewall_on - включение и настройка firewall. Разрешенные порты указаны в папке groups_vars
- firewall_off - отключение firewall.

Пример использования в vagrant provisioner:

```ruby
box.vm.provision "ansible" do |ansible|
        ansible.inventory_path = "ansible/hosts"
        ansible.host_key_checking = "false"
        ansible.playbook = "ansible/install.yml"

        if boxconfig[:vm_name] == "monitoring"
         ansible.tags = ["install_prom", "install_grafana_rpm", "settings_grafana", "rsyslog-client", "install_node_exp", "firewall_on"]
        
        elsif boxconfig[:vm_name] == "backend2"
          ansible.tags = ["install_mongo", "enable_auth", "mongo_conf", "install_node", "create_service", "copy_app", "rsyslog-client", "install_node_exp", "firewall_on"]

        elsif boxconfig[:vm_name] == "backup"
          ansible.tags = ["install_mongo", "enable_auth", "mongo_conf", "backup", "rsyslog-srv", "install_node_exp", "firewall_on"]

        elsif boxconfig[:vm_name] == "backend1"
          ansible.tags = ["install_mongo", "enable_auth", "mongo_conf", "enable_repl", "install_node", "create_service", "copy_app", "rsyslog-client", "install_node_exp", "firewall_on"]
          
        elsif boxconfig[:vm_name] == "frontend"
          ansible.tags = ["install_nginx", "syn_fold", "nginx_cfg", "nginx_selinux", "rsyslog-client", "install_node_exp", "firewall_on"]
        end
```

Пример использования в bash:

```bash
ansible-playbook install.yml -l backend1 --tags install_mongo,enable_auth,mongo_conf,enable_repl,install_node,create_service,copy_app,rsyslog-client
```

### 3.5 Восстановление сервисов

Для пересоздания какого либо сервера frontend, backend1, backend2, backup, monitoring необходимо внести изменения в wp-app.tf.  
Удалить блок отвечающий за сервер. Запустить `terraform apply`. Затем вернуть записи о данном сервере. Выполнить `terraform apply`.    
Запустить install.yml для восстановления сервера.  

Пример восстановлениея backend1 (primary член репликации)

Удаляем в wb-app.tf:

```HCL
resource "yandex_compute_instance" "backend1" {
  ...
}
```

```bash
terraform apply --auto-approve
```

Возращаем в wb-app.tf:

```HCL
resource "yandex_compute_instance" "backend1" {
  ...
}
```

```bash
terraform apply --auto-approve
```

Запускаем install.yml: (тэг enable_repl не используем, так как репликация уже включена на уровне группы)

```bash
ansible-playbook install.yml -l backend1 --tags install_mongo,enable_auth,mongo_conf,install_node,create_service,copy_app,rsyslog-client
```

В случае восстановления backend2, backup, которые являются secondary участниками репликации mongodb,  
база автоматически произведет репликацию на восстановленный сервер.  
При восстановление сервера backend1, который является primary участником репликации mongodb, база автоматически произведет репликацию, и backend1 станет primary.  
За автоматическое переключение запросов на резервный бэкенд в случае отказа отвечает nginx сервер:

```nginx
upstream backend {
    server {{ backend_name }}:{{ backend_port }} fail_timeout=3s max_fails=2;
    server {{ backend_name_back }}:{{ backend_port }} backup;
}
```

Также стоит учесть, что при восстановление backend1, в момент provision сервера, которое занимает время, так как сервер включен и доступен,  
nginx будет считать сервер доступным (хотя сервер еще устаналивается ) и секция:
``server {{ backend_name }}:{{ backend_port }} fail_timeout=3s max_fails=2;`` будет ввести на еще не готовый сервис.  
Чтобы этого не происходило, можно воспользоваться скиптом ``change_app.sh`` переключающим на другой бэкенд.  
Либо производить восстановление в часы для технического обслуживания (бэкенд будет не доступен).

```bash
cd ansible
user:~/sports-helper-service/ansible$ ./change_app.sh
Выберите хост для переключения backend:

1. backend1

2. backend2
```

После восстановления вернуть основной бэкенд скриптом ./change_app.sh (если скрипт использовался для переключения на backend2)
В случае неполадок с бэкендом на серверах можно использовать скрипт `restart_backend.sh` он перезапустит приложение sports-helper на выбранном хосте.

### 3.6 Карта ansible

```bash
.
├── ansible.cfg
├── backend1.yml
├── backend2.yml
├── backup.yml
├── change_app.sh
├── frontend.yml
├── group_vars
│   ├── backup_servers.yml
│   ├── clients.yml
│   ├── mongo_primary.yml
│   ├── mongo_secondary.yml
│   └── monitoring.yml
├── hosts
├── infra.yml
├── install.yml
├── local_playbooks
│   ├── add_hosts.yml
│   ├── mongo_key.yml
│   ├── nginx_cert.yml
│   └── wget_grafana.yml
├── local.yml
├── monitoring.yml
├── restart_backend2.yml
├── restart_backend.sh
├── roles
│   ├── backup
│   ├── firewalld
│   ├── logs
│   ├── mongodb
│   ├── monitoring
│   ├── nginx
│   └── nodejs
└── yc2inv.py
```

## 4. Настройка terraform и gitlab для проекта

Настроим хранения состояния terraform на gitlab

```bash
cd terraform
```

Чтобы не было проблем с блокировкой hashicorp, будем использовать .terraformrc:

```hcl
provider_installation {
  network_mirror {
    url = "https://terraform-mirror.yandexcloud.net/"
    include = ["registry.terraform.io/*/*"]
  }
  direct {
    exclude = ["registry.terraform.io/*/*"]
  }
}
```

Необходимо расположить данный файл в домашней папке пользователя.

```bash
cp .terraformrc ~
```

Создадим backend.tf (файл необходимый для создания состояния tfstate)

```tf
terraform {
  backend "http" {
  }
}
```

Создадим token (YOUR-ACCESS-TOKEN) для api в gitlab (<https://gitlab.com/-/user_settings/personal_access_tokens>) и выдадим права для данного token. 

Создадим tfstate для prod среды, перейдем в меню terraform states в gitlab и скопиируем скрипт иницилизации:

```bash
cd terraform/prod
export GITLAB_ACCESS_TOKEN="token"
export TF_STATE_NAME="iac_prod"
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/54270053/terraform/state/$TF_STATE_NAME" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/54270053/terraform/state/$TF_STATE_NAME/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/54270053/terraform/state/$TF_STATE_NAME/lock" \
    -backend-config="username=timofeevms" \
    -backend-config="password=$GITLAB_ACCESS_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```

Создадим tfstate для stage среды:

```bash
cd ../stage
export TF_STATE_NAME="iac_stage"
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/54270053/terraform/state/$TF_STATE_NAME" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/54270053/terraform/state/$TF_STATE_NAME/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/54270053/terraform/state/$TF_STATE_NAME/lock" \
    -backend-config="username=timofeevms" \
    -backend-config="password=$GITLAB_ACCESS_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```

Создадим переменные необходимые для ci/cd pipeline - setting - ci/cd - variables

![Alt text](images/variables.png)

Описание переменных:

- ID_RSA приватная часть ключа, необходим для контейнера с ansible, чтобы подключаться по ssh к хостам
- ID_RSA_PUB публичная часть ключа, необходима для контейнера с terraform, чтобы создовать хосты в yacloud с ssh ключом
- TF_HTTP_PASSWORD это GITLAB_ACCESS_TOKEN для доступа контейнера с terraform к хранилищу gitlab terraform state
- yc_cloud - id yandex облака необходим для контейнера с terraform
- yc_folder_prod - id папки prod серверов необходим для terraform и ansible контейнеров
- yc_folder_stage - id папки stage серверов необходим для terraform и ansible контейнеров
- yc_token - token для yandex облака необходим для terraform и ansible контейнеров

Для работы на локальном компьютере с terraform должен быть файл wp.auto.tfvars (занесен в .gitignore) со значениями переменных для yacloud:

```hcl
yc_cloud  = "id cloud"
yc_folder = "id_folder"
yc_token = "token"
```

## 5. Проверка

Добавим ip адреса prod и stage серверов в файл hosts на локальной машине.

```txt
158.160.100.202 sports-helper
158.160.109.124 monitoring
158.160.101.156 sports-helper-stage
158.160.49.103 monitoring-stage
```

Проверим доступность веб сервисов.

![Alt text](images/proverka.png)

Ссылка на финальный pipeline

<https://gitlab.com/otus_iac/sports-helper/-/pipelines/1153253207>

Презентация Pdf доступна по адресу:

[Презентация sports-helper](sports-helper.pdf)
